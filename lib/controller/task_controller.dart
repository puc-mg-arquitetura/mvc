
import 'package:mvc/model/task.dart';

class TaskController {
  List<Task> _tasks = [];

  List<Task> get tasks => _tasks;

  void addTask(String title) {
    _tasks.add(Task(title: title));
  }

  void toggleTask(Task task) {
    task.toggleDone();
  }

  void deleteTask(Task task) {
    _tasks.remove(task);
  }
}
